#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/statix_nashc.mk

COMMON_LUNCH_CHOICES := \
    statix_nashc-user \
    statix_nashc-userdebug \
    statix_nashc-eng
